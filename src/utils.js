export const reduceArray = (array, newSize) => {
  const returnArray = [];
  const valuesToSum = array.length / newSize;
  for (let i = 0; i < array.length; i += valuesToSum) {
    let sum = 0;
    let j;
    let start_i = Math.floor(i);
    for (j = start_i; j < Math.min(start_i + valuesToSum, array.length); j++) {
      sum += array[j];
    }
    returnArray.push(sum / (j - start_i));
  }
  return returnArray;
}