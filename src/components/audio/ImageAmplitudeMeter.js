import { CONFIG } from "../../config.js";

export class ImageAmplitudeMeter {
  constructor(
    imgPath,
    x = CONFIG.width / 2,
    y = CONFIG.height / 2,
    maxSize = 100
  ) {
    this.image = loadImage(imgPath);
    this.CENTER_X = x;
    this.CENTER_Y = y;
    this.maxSize = maxSize;
  }

  draw(multiplier = 0) {
    image(
      this.image,
      this.CENTER_X - (this.maxSize * multiplier) / 2,
      this.CENTER_Y - (this.maxSize * multiplier) / 2,
      this.maxSize * multiplier,
      this.maxSize * multiplier
    );
  }
}
