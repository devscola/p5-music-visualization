import { Spectrum } from "./Spectrum.js";
import { CONFIG } from "../../config.js";

const defaultConfig = {
  numberOfBars: 50
};

class Bar {
  constructor(x, y, width, height, color) {
    this.x = x;
    this.y = y;
    this.height = height;
    this.width = width;
    this.color = color;
  }

  draw() {
    // noStroke();
    noFill()
    stroke(255)
    colorMode(HSB);
    // fill(this.color, 100, 100);
    rect(this.x, this.y, this.width, this.height);
  }
}

export class BarsEqualizerVertical {
  constructor(config = defaultConfig) {
    this.config = { ...defaultConfig, ...config };
  }

  draw(fullSpectrum) {
    const spectrum = new Spectrum(fullSpectrum)
      .trim(-300)
      .simplify(this.config.numberOfBars)
      .asArray();

    const numberOfBars = spectrum.length;

    spectrum.forEach((frequency, index) => {
      const barHeight = -height + map(frequency, 0, 100, CONFIG.height, 200);
      const barWidth = width / numberOfBars;
      const xCenter = map(index, 0, numberOfBars, 0, CONFIG.width);
      const xPos = xCenter + barWidth / 2
      const yPos = height + barHeight/2;
      const hue = 360/numberOfBars * index

      new Bar( yPos, xPos, barHeight, barWidth, hue).draw();
      new Bar( yPos + 10, xPos + 10, barHeight, barWidth, hue).draw();
      new Bar( yPos + 25, xPos + 35, barHeight, barWidth, hue).draw();
      new Bar( yPos + 1500, xPos + 15, barHeight, barWidth, hue).draw();
      new Bar( yPos + 1490, xPos + 25, barHeight, barWidth, hue).draw();
      new Bar( yPos + 1505, xPos + 38, barHeight, barWidth, hue).draw();
    });
  }
}
