FROM node:13.13.0-alpine3.10

WORKDIR /app

COPY . .

RUN npm install

CMD ["npm", "run", "dev"]

#docker run -it -v $(pwd):/app --workdir /app -p 8080:8080 node:13.13.0-alpine3.10 npm install && npm run dev